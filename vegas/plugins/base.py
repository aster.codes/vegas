from discord.ext import commands

"""
BasePlugin

Used as a plugin base for all VEGAS Plugins
"""

class BasePlugin(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
