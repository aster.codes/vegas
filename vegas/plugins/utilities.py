import asyncio
import discord
from discord.utils import get
from discord.ext import commands
from discord.ext.commands import has_any_role

from .base import BasePlugin
from utils import get_role




class UtilitiesPlugin(BasePlugin):
    @commands.command(hidden=True)
    async def thumbs(self, context):
        await context.send("Pelase thumb up!")
        def check(reaction, user):
            return user == context.message.author and str(reaction.emoji) in ['👍', '🗿', '📄', '✂']
        try:
            reaction, user = await self.bot.wait_for('reaction_add', timeout=60.0, check=check)
            await context.send(reaction)
        except asyncio.TimeoutError:
            await context.send('👎')


    @commands.command(description="How many bans until Debs/1000?",
                      case_insensitive=True)
    @commands.cooldown(1, 60, type=commands.BucketType.guild)
    async def how_many_bans(self, context):
        current_bans = await context.guild.bans()
        bans_left = 1000 - int(len(current_bans))
        await context.send(":rainbow: Only " + str(bans_left) + " Degens left until 1000! :rainbow:")


    @commands.command(description="Returns all of the users that have a \
                                   specific role",
                      case_insensitive=True)
    @has_any_role('Mods', 'Disaster Director', 'Subreddit Moderator',
                  'Server Glue')
    async def rolemembers(self, context, roleArg=None):
        role_list = [role.name for role in context.message.guild.roles]
        if roleArg is None:
            await context.send("You didn't add a role.")
        else:
            newRole = await get_role(context, roleArg)
            if newRole.name.lower() in [x.lower() for x in role_list]:
                memberNames = [x.name + "#" + x.discriminator
                               for x in newRole.members]
                title = 'Members with ' + newRole.name
                description = '-'+'\n-'.join(memberNames)
                colour = newRole.color
                embed = discord.Embed(title=title, description=description,
                                      colour=colour)

                if len(description) > 1950:
                    await context.send("There are too many users with this role\
                            to display!")
                else:
                    await context.send(embed=embed)
            else:
                await context.send("That argument isn't in the rolelist.")


    @commands.command(hidden=True, case_insensitive=True)
    async def i_will_follow_the_rules(self, context):
        await context.message.delete()
        role = await get_role(context, "Haven't Read the Rules")
        await context.message.author.remove_roles(role, 
                                reason="User {} agreed to the Rules".format(
                                    context.message.author))
        await context.send(context.author.mention + ", thanks for reading the rules! Enjoy the server!")

    

def setup(bot):
    bot.add_cog(UtilitiesPlugin(bot))
